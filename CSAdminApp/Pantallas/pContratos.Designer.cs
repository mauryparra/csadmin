﻿namespace CSAdminApp.Pantallas
{
    partial class pContratos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlContratos = new System.Windows.Forms.TabControl();
            this.tabPageAlta = new System.Windows.Forms.TabPage();
            this.aButtonCancelar = new System.Windows.Forms.Button();
            this.aButtonGuardar = new System.Windows.Forms.Button();
            this.aGroupBoxContrato = new System.Windows.Forms.GroupBox();
            this.aGroupBoxDestino = new System.Windows.Forms.GroupBox();
            this.aNumericUpDownHoras = new System.Windows.Forms.NumericUpDown();
            this.aLabelSede = new System.Windows.Forms.Label();
            this.aTextBoxSede = new System.Windows.Forms.TextBox();
            this.aLabelHoras = new System.Windows.Forms.Label();
            this.aTextBoxObs = new System.Windows.Forms.TextBox();
            this.aLabelObs = new System.Windows.Forms.Label();
            this.aComboBoxFuncion = new System.Windows.Forms.ComboBox();
            this.aLabelFuncion = new System.Windows.Forms.Label();
            this.aComboBoxEquipo = new System.Windows.Forms.ComboBox();
            this.aLabelEquipo = new System.Windows.Forms.Label();
            this.aGroupBoxSitPro = new System.Windows.Forms.GroupBox();
            this.aComboBoxCargo = new System.Windows.Forms.ComboBox();
            this.aComboBoxCondicion = new System.Windows.Forms.ComboBox();
            this.aLabelCargo = new System.Windows.Forms.Label();
            this.aLabelCondicion = new System.Windows.Forms.Label();
            this.aGroupBoxOrigen = new System.Windows.Forms.GroupBox();
            this.aComboBoxOrigen = new System.Windows.Forms.ComboBox();
            this.aCheckBoxAfectado = new System.Windows.Forms.CheckBox();
            this.aDateTimePickerBaja = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.aDateTimePickerInicio = new System.Windows.Forms.DateTimePicker();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.aGroupBoxPersona = new System.Windows.Forms.GroupBox();
            this.aTextBoxApellido = new System.Windows.Forms.TextBox();
            this.aLabelApellido = new System.Windows.Forms.Label();
            this.aTextBoxNombre = new System.Windows.Forms.TextBox();
            this.aLabelNombre = new System.Windows.Forms.Label();
            this.aMaskedTextBoxCuit = new System.Windows.Forms.MaskedTextBox();
            this.aLabelCuit = new System.Windows.Forms.Label();
            this.aMaskedTextBoxDNI = new System.Windows.Forms.MaskedTextBox();
            this.aLabelDNI = new System.Windows.Forms.Label();
            this.tabPageModificar = new System.Windows.Forms.TabPage();
            this.tabPageEliminar = new System.Windows.Forms.TabPage();
            this.tabControlContratos.SuspendLayout();
            this.tabPageAlta.SuspendLayout();
            this.aGroupBoxContrato.SuspendLayout();
            this.aGroupBoxDestino.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aNumericUpDownHoras)).BeginInit();
            this.aGroupBoxSitPro.SuspendLayout();
            this.aGroupBoxOrigen.SuspendLayout();
            this.aGroupBoxPersona.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlContratos
            // 
            this.tabControlContratos.Controls.Add(this.tabPageAlta);
            this.tabControlContratos.Controls.Add(this.tabPageModificar);
            this.tabControlContratos.Controls.Add(this.tabPageEliminar);
            this.tabControlContratos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlContratos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tabControlContratos.Location = new System.Drawing.Point(0, 0);
            this.tabControlContratos.Name = "tabControlContratos";
            this.tabControlContratos.SelectedIndex = 0;
            this.tabControlContratos.Size = new System.Drawing.Size(689, 446);
            this.tabControlContratos.TabIndex = 0;
            // 
            // tabPageAlta
            // 
            this.tabPageAlta.Controls.Add(this.aButtonCancelar);
            this.tabPageAlta.Controls.Add(this.aButtonGuardar);
            this.tabPageAlta.Controls.Add(this.aGroupBoxContrato);
            this.tabPageAlta.Controls.Add(this.aGroupBoxPersona);
            this.tabPageAlta.Location = new System.Drawing.Point(4, 24);
            this.tabPageAlta.Name = "tabPageAlta";
            this.tabPageAlta.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAlta.Size = new System.Drawing.Size(681, 418);
            this.tabPageAlta.TabIndex = 0;
            this.tabPageAlta.Text = "Alta";
            this.tabPageAlta.UseVisualStyleBackColor = true;
            // 
            // aButtonCancelar
            // 
            this.aButtonCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.aButtonCancelar.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightSeaGreen;
            this.aButtonCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepPink;
            this.aButtonCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.HotPink;
            this.aButtonCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aButtonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aButtonCancelar.Location = new System.Drawing.Point(570, 388);
            this.aButtonCancelar.Name = "aButtonCancelar";
            this.aButtonCancelar.Size = new System.Drawing.Size(88, 26);
            this.aButtonCancelar.TabIndex = 40;
            this.aButtonCancelar.Text = "&Cancelar";
            this.aButtonCancelar.UseVisualStyleBackColor = true;
            this.aButtonCancelar.Click += new System.EventHandler(this.aButtonCancelar_Click);
            // 
            // aButtonGuardar
            // 
            this.aButtonGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.aButtonGuardar.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightSeaGreen;
            this.aButtonGuardar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSeaGreen;
            this.aButtonGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumTurquoise;
            this.aButtonGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aButtonGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aButtonGuardar.Location = new System.Drawing.Point(476, 388);
            this.aButtonGuardar.Name = "aButtonGuardar";
            this.aButtonGuardar.Size = new System.Drawing.Size(88, 26);
            this.aButtonGuardar.TabIndex = 39;
            this.aButtonGuardar.Text = "&Guardar";
            this.aButtonGuardar.UseVisualStyleBackColor = true;
            this.aButtonGuardar.Click += new System.EventHandler(this.aButtonGuardar_Click);
            // 
            // aGroupBoxContrato
            // 
            this.aGroupBoxContrato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aGroupBoxContrato.Controls.Add(this.aGroupBoxDestino);
            this.aGroupBoxContrato.Controls.Add(this.aGroupBoxSitPro);
            this.aGroupBoxContrato.Controls.Add(this.aGroupBoxOrigen);
            this.aGroupBoxContrato.Controls.Add(this.aDateTimePickerBaja);
            this.aGroupBoxContrato.Controls.Add(this.label2);
            this.aGroupBoxContrato.Controls.Add(this.label1);
            this.aGroupBoxContrato.Controls.Add(this.aDateTimePickerInicio);
            this.aGroupBoxContrato.Controls.Add(this.shapeContainer2);
            this.aGroupBoxContrato.Location = new System.Drawing.Point(17, 129);
            this.aGroupBoxContrato.Name = "aGroupBoxContrato";
            this.aGroupBoxContrato.Size = new System.Drawing.Size(641, 254);
            this.aGroupBoxContrato.TabIndex = 2;
            this.aGroupBoxContrato.TabStop = false;
            this.aGroupBoxContrato.Text = "Contrato";
            // 
            // aGroupBoxDestino
            // 
            this.aGroupBoxDestino.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aGroupBoxDestino.Controls.Add(this.aNumericUpDownHoras);
            this.aGroupBoxDestino.Controls.Add(this.aLabelSede);
            this.aGroupBoxDestino.Controls.Add(this.aTextBoxSede);
            this.aGroupBoxDestino.Controls.Add(this.aLabelHoras);
            this.aGroupBoxDestino.Controls.Add(this.aTextBoxObs);
            this.aGroupBoxDestino.Controls.Add(this.aLabelObs);
            this.aGroupBoxDestino.Controls.Add(this.aComboBoxFuncion);
            this.aGroupBoxDestino.Controls.Add(this.aLabelFuncion);
            this.aGroupBoxDestino.Controls.Add(this.aComboBoxEquipo);
            this.aGroupBoxDestino.Controls.Add(this.aLabelEquipo);
            this.aGroupBoxDestino.Location = new System.Drawing.Point(9, 134);
            this.aGroupBoxDestino.Name = "aGroupBoxDestino";
            this.aGroupBoxDestino.Size = new System.Drawing.Size(626, 115);
            this.aGroupBoxDestino.TabIndex = 6;
            this.aGroupBoxDestino.TabStop = false;
            this.aGroupBoxDestino.Text = "Destino";
            // 
            // aNumericUpDownHoras
            // 
            this.aNumericUpDownHoras.BackColor = System.Drawing.Color.MistyRose;
            this.aNumericUpDownHoras.Location = new System.Drawing.Point(385, 50);
            this.aNumericUpDownHoras.Maximum = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.aNumericUpDownHoras.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.aNumericUpDownHoras.Name = "aNumericUpDownHoras";
            this.aNumericUpDownHoras.Size = new System.Drawing.Size(68, 21);
            this.aNumericUpDownHoras.TabIndex = 9;
            this.aNumericUpDownHoras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.aNumericUpDownHoras.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // aLabelSede
            // 
            this.aLabelSede.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aLabelSede.AutoSize = true;
            this.aLabelSede.Location = new System.Drawing.Point(325, 22);
            this.aLabelSede.Name = "aLabelSede";
            this.aLabelSede.Size = new System.Drawing.Size(39, 15);
            this.aLabelSede.TabIndex = 2;
            this.aLabelSede.Text = "Sede:";
            // 
            // aTextBoxSede
            // 
            this.aTextBoxSede.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aTextBoxSede.Location = new System.Drawing.Point(385, 19);
            this.aTextBoxSede.Name = "aTextBoxSede";
            this.aTextBoxSede.ReadOnly = true;
            this.aTextBoxSede.Size = new System.Drawing.Size(232, 21);
            this.aTextBoxSede.TabIndex = 3;
            this.aTextBoxSede.TabStop = false;
            // 
            // aLabelHoras
            // 
            this.aLabelHoras.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aLabelHoras.AutoSize = true;
            this.aLabelHoras.Location = new System.Drawing.Point(325, 52);
            this.aLabelHoras.Name = "aLabelHoras";
            this.aLabelHoras.Size = new System.Drawing.Size(43, 15);
            this.aLabelHoras.TabIndex = 8;
            this.aLabelHoras.Text = "Horas:";
            // 
            // aTextBoxObs
            // 
            this.aTextBoxObs.BackColor = System.Drawing.Color.Honeydew;
            this.aTextBoxObs.Location = new System.Drawing.Point(93, 80);
            this.aTextBoxObs.Name = "aTextBoxObs";
            this.aTextBoxObs.Size = new System.Drawing.Size(211, 21);
            this.aTextBoxObs.TabIndex = 7;
            // 
            // aLabelObs
            // 
            this.aLabelObs.AutoSize = true;
            this.aLabelObs.Location = new System.Drawing.Point(9, 83);
            this.aLabelObs.Name = "aLabelObs";
            this.aLabelObs.Size = new System.Drawing.Size(78, 15);
            this.aLabelObs.TabIndex = 6;
            this.aLabelObs.Text = "Observación:";
            // 
            // aComboBoxFuncion
            // 
            this.aComboBoxFuncion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.aComboBoxFuncion.BackColor = System.Drawing.Color.MistyRose;
            this.aComboBoxFuncion.DisplayMember = "Funciones.Id";
            this.aComboBoxFuncion.FormattingEnabled = true;
            this.aComboBoxFuncion.Location = new System.Drawing.Point(93, 49);
            this.aComboBoxFuncion.Name = "aComboBoxFuncion";
            this.aComboBoxFuncion.Size = new System.Drawing.Size(157, 23);
            this.aComboBoxFuncion.TabIndex = 5;
            this.aComboBoxFuncion.ValueMember = "Funciones.Id";
            // 
            // aLabelFuncion
            // 
            this.aLabelFuncion.AutoSize = true;
            this.aLabelFuncion.Location = new System.Drawing.Point(9, 52);
            this.aLabelFuncion.Name = "aLabelFuncion";
            this.aLabelFuncion.Size = new System.Drawing.Size(54, 15);
            this.aLabelFuncion.TabIndex = 4;
            this.aLabelFuncion.Text = "Función:";
            // 
            // aComboBoxEquipo
            // 
            this.aComboBoxEquipo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.aComboBoxEquipo.BackColor = System.Drawing.Color.MistyRose;
            this.aComboBoxEquipo.DisplayMember = "Equipos.Id";
            this.aComboBoxEquipo.FormattingEnabled = true;
            this.aComboBoxEquipo.Location = new System.Drawing.Point(93, 19);
            this.aComboBoxEquipo.Name = "aComboBoxEquipo";
            this.aComboBoxEquipo.Size = new System.Drawing.Size(157, 23);
            this.aComboBoxEquipo.TabIndex = 1;
            this.aComboBoxEquipo.ValueMember = "Equipos.Id";
            this.aComboBoxEquipo.SelectedIndexChanged += new System.EventHandler(this.aComboBoxEquipo_SelectedIndexChanged);
            // 
            // aLabelEquipo
            // 
            this.aLabelEquipo.AutoSize = true;
            this.aLabelEquipo.Location = new System.Drawing.Point(9, 22);
            this.aLabelEquipo.Name = "aLabelEquipo";
            this.aLabelEquipo.Size = new System.Drawing.Size(49, 15);
            this.aLabelEquipo.TabIndex = 0;
            this.aLabelEquipo.Text = "Equipo:";
            // 
            // aGroupBoxSitPro
            // 
            this.aGroupBoxSitPro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aGroupBoxSitPro.Controls.Add(this.aComboBoxCargo);
            this.aGroupBoxSitPro.Controls.Add(this.aComboBoxCondicion);
            this.aGroupBoxSitPro.Controls.Add(this.aLabelCargo);
            this.aGroupBoxSitPro.Controls.Add(this.aLabelCondicion);
            this.aGroupBoxSitPro.Location = new System.Drawing.Point(328, 59);
            this.aGroupBoxSitPro.Name = "aGroupBoxSitPro";
            this.aGroupBoxSitPro.Size = new System.Drawing.Size(307, 69);
            this.aGroupBoxSitPro.TabIndex = 5;
            this.aGroupBoxSitPro.TabStop = false;
            this.aGroupBoxSitPro.Text = "Situacion de Revista";
            // 
            // aComboBoxCargo
            // 
            this.aComboBoxCargo.BackColor = System.Drawing.Color.MistyRose;
            this.aComboBoxCargo.FormattingEnabled = true;
            this.aComboBoxCargo.Location = new System.Drawing.Point(55, 22);
            this.aComboBoxCargo.Name = "aComboBoxCargo";
            this.aComboBoxCargo.Size = new System.Drawing.Size(79, 23);
            this.aComboBoxCargo.TabIndex = 1;
            this.aComboBoxCargo.SelectedIndexChanged += new System.EventHandler(this.aComboBoxCargo_SelectedIndexChanged);
            // 
            // aComboBoxCondicion
            // 
            this.aComboBoxCondicion.BackColor = System.Drawing.Color.MistyRose;
            this.aComboBoxCondicion.FormattingEnabled = true;
            this.aComboBoxCondicion.Location = new System.Drawing.Point(219, 22);
            this.aComboBoxCondicion.Name = "aComboBoxCondicion";
            this.aComboBoxCondicion.Size = new System.Drawing.Size(79, 23);
            this.aComboBoxCondicion.TabIndex = 3;
            // 
            // aLabelCargo
            // 
            this.aLabelCargo.AutoSize = true;
            this.aLabelCargo.Location = new System.Drawing.Point(6, 25);
            this.aLabelCargo.Name = "aLabelCargo";
            this.aLabelCargo.Size = new System.Drawing.Size(43, 15);
            this.aLabelCargo.TabIndex = 0;
            this.aLabelCargo.Text = "Cargo:";
            // 
            // aLabelCondicion
            // 
            this.aLabelCondicion.AutoSize = true;
            this.aLabelCondicion.Location = new System.Drawing.Point(148, 25);
            this.aLabelCondicion.Name = "aLabelCondicion";
            this.aLabelCondicion.Size = new System.Drawing.Size(65, 15);
            this.aLabelCondicion.TabIndex = 2;
            this.aLabelCondicion.Text = "Condición:";
            // 
            // aGroupBoxOrigen
            // 
            this.aGroupBoxOrigen.Controls.Add(this.aComboBoxOrigen);
            this.aGroupBoxOrigen.Controls.Add(this.aCheckBoxAfectado);
            this.aGroupBoxOrigen.Location = new System.Drawing.Point(9, 59);
            this.aGroupBoxOrigen.Name = "aGroupBoxOrigen";
            this.aGroupBoxOrigen.Size = new System.Drawing.Size(304, 69);
            this.aGroupBoxOrigen.TabIndex = 4;
            this.aGroupBoxOrigen.TabStop = false;
            this.aGroupBoxOrigen.Text = "Origen";
            // 
            // aComboBoxOrigen
            // 
            this.aComboBoxOrigen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.aComboBoxOrigen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.aComboBoxOrigen.BackColor = System.Drawing.Color.Honeydew;
            this.aComboBoxOrigen.Enabled = false;
            this.aComboBoxOrigen.FormattingEnabled = true;
            this.aComboBoxOrigen.Items.AddRange(new object[] {
            "Ministerio de Educación",
            "Ministerio de Salud",
            "Ministerio de Infraestructura"});
            this.aComboBoxOrigen.Location = new System.Drawing.Point(85, 25);
            this.aComboBoxOrigen.Name = "aComboBoxOrigen";
            this.aComboBoxOrigen.Size = new System.Drawing.Size(213, 23);
            this.aComboBoxOrigen.TabIndex = 1;
            // 
            // aCheckBoxAfectado
            // 
            this.aCheckBoxAfectado.AutoSize = true;
            this.aCheckBoxAfectado.BackColor = System.Drawing.Color.Honeydew;
            this.aCheckBoxAfectado.Location = new System.Drawing.Point(6, 27);
            this.aCheckBoxAfectado.Name = "aCheckBoxAfectado";
            this.aCheckBoxAfectado.Size = new System.Drawing.Size(73, 19);
            this.aCheckBoxAfectado.TabIndex = 0;
            this.aCheckBoxAfectado.Text = "Afectado";
            this.aCheckBoxAfectado.UseVisualStyleBackColor = false;
            this.aCheckBoxAfectado.CheckedChanged += new System.EventHandler(this.aCheckBoxAfectado_CheckedChanged);
            // 
            // aDateTimePickerBaja
            // 
            this.aDateTimePickerBaja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aDateTimePickerBaja.Checked = false;
            this.aDateTimePickerBaja.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.aDateTimePickerBaja.Location = new System.Drawing.Point(442, 32);
            this.aDateTimePickerBaja.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.aDateTimePickerBaja.Name = "aDateTimePickerBaja";
            this.aDateTimePickerBaja.ShowCheckBox = true;
            this.aDateTimePickerBaja.Size = new System.Drawing.Size(99, 21);
            this.aDateTimePickerBaja.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(357, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fecha Baja:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fecha Inicio:";
            // 
            // aDateTimePickerInicio
            // 
            this.aDateTimePickerInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.aDateTimePickerInicio.Location = new System.Drawing.Point(88, 32);
            this.aDateTimePickerInicio.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.aDateTimePickerInicio.Name = "aDateTimePickerInicio";
            this.aDateTimePickerInicio.Size = new System.Drawing.Size(99, 21);
            this.aDateTimePickerInicio.TabIndex = 0;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 17);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(635, 234);
            this.shapeContainer2.TabIndex = 7;
            this.shapeContainer2.TabStop = false;
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rectangleShape2.BackColor = System.Drawing.Color.Transparent;
            this.rectangleShape2.BorderColor = System.Drawing.Color.Aquamarine;
            this.rectangleShape2.BorderWidth = 3;
            this.rectangleShape2.Location = new System.Drawing.Point(437, 12);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(102, 24);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BackColor = System.Drawing.Color.Transparent;
            this.rectangleShape1.BorderColor = System.Drawing.Color.MistyRose;
            this.rectangleShape1.BorderWidth = 3;
            this.rectangleShape1.Location = new System.Drawing.Point(84, 13);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(102, 24);
            // 
            // aGroupBoxPersona
            // 
            this.aGroupBoxPersona.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aGroupBoxPersona.Controls.Add(this.aTextBoxApellido);
            this.aGroupBoxPersona.Controls.Add(this.aLabelApellido);
            this.aGroupBoxPersona.Controls.Add(this.aTextBoxNombre);
            this.aGroupBoxPersona.Controls.Add(this.aLabelNombre);
            this.aGroupBoxPersona.Controls.Add(this.aMaskedTextBoxCuit);
            this.aGroupBoxPersona.Controls.Add(this.aLabelCuit);
            this.aGroupBoxPersona.Controls.Add(this.aMaskedTextBoxDNI);
            this.aGroupBoxPersona.Controls.Add(this.aLabelDNI);
            this.aGroupBoxPersona.Location = new System.Drawing.Point(17, 6);
            this.aGroupBoxPersona.Name = "aGroupBoxPersona";
            this.aGroupBoxPersona.Size = new System.Drawing.Size(641, 117);
            this.aGroupBoxPersona.TabIndex = 1;
            this.aGroupBoxPersona.TabStop = false;
            this.aGroupBoxPersona.Text = "Persona";
            // 
            // aTextBoxApellido
            // 
            this.aTextBoxApellido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aTextBoxApellido.Location = new System.Drawing.Point(420, 62);
            this.aTextBoxApellido.Name = "aTextBoxApellido";
            this.aTextBoxApellido.ReadOnly = true;
            this.aTextBoxApellido.Size = new System.Drawing.Size(154, 21);
            this.aTextBoxApellido.TabIndex = 7;
            this.aTextBoxApellido.TabStop = false;
            // 
            // aLabelApellido
            // 
            this.aLabelApellido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aLabelApellido.AutoSize = true;
            this.aLabelApellido.Location = new System.Drawing.Point(354, 65);
            this.aLabelApellido.Name = "aLabelApellido";
            this.aLabelApellido.Size = new System.Drawing.Size(60, 15);
            this.aLabelApellido.TabIndex = 6;
            this.aLabelApellido.Text = "Apellidos:";
            // 
            // aTextBoxNombre
            // 
            this.aTextBoxNombre.Location = new System.Drawing.Point(73, 62);
            this.aTextBoxNombre.Name = "aTextBoxNombre";
            this.aTextBoxNombre.ReadOnly = true;
            this.aTextBoxNombre.Size = new System.Drawing.Size(154, 21);
            this.aTextBoxNombre.TabIndex = 5;
            this.aTextBoxNombre.TabStop = false;
            // 
            // aLabelNombre
            // 
            this.aLabelNombre.AutoSize = true;
            this.aLabelNombre.Location = new System.Drawing.Point(6, 65);
            this.aLabelNombre.Name = "aLabelNombre";
            this.aLabelNombre.Size = new System.Drawing.Size(61, 15);
            this.aLabelNombre.TabIndex = 4;
            this.aLabelNombre.Text = "Nombres:";
            // 
            // aMaskedTextBoxCuit
            // 
            this.aMaskedTextBoxCuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aMaskedTextBoxCuit.Location = new System.Drawing.Point(420, 27);
            this.aMaskedTextBoxCuit.Mask = "99-99999999-9";
            this.aMaskedTextBoxCuit.Name = "aMaskedTextBoxCuit";
            this.aMaskedTextBoxCuit.PromptChar = ' ';
            this.aMaskedTextBoxCuit.ReadOnly = true;
            this.aMaskedTextBoxCuit.Size = new System.Drawing.Size(121, 21);
            this.aMaskedTextBoxCuit.TabIndex = 3;
            this.aMaskedTextBoxCuit.TabStop = false;
            this.aMaskedTextBoxCuit.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // aLabelCuit
            // 
            this.aLabelCuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aLabelCuit.AutoSize = true;
            this.aLabelCuit.Location = new System.Drawing.Point(357, 30);
            this.aLabelCuit.Name = "aLabelCuit";
            this.aLabelCuit.Size = new System.Drawing.Size(37, 15);
            this.aLabelCuit.TabIndex = 2;
            this.aLabelCuit.Text = "CUIT:";
            // 
            // aMaskedTextBoxDNI
            // 
            this.aMaskedTextBoxDNI.BackColor = System.Drawing.Color.MistyRose;
            this.aMaskedTextBoxDNI.Location = new System.Drawing.Point(73, 27);
            this.aMaskedTextBoxDNI.Mask = "99.999.999";
            this.aMaskedTextBoxDNI.Name = "aMaskedTextBoxDNI";
            this.aMaskedTextBoxDNI.PromptChar = ' ';
            this.aMaskedTextBoxDNI.Size = new System.Drawing.Size(124, 21);
            this.aMaskedTextBoxDNI.TabIndex = 1;
            this.aMaskedTextBoxDNI.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.aMaskedTextBoxDNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aMaskedTextBoxDNI_KeyPress);
            this.aMaskedTextBoxDNI.Validating += new System.ComponentModel.CancelEventHandler(this.aMaskedTextBoxDNI_Validating);
            // 
            // aLabelDNI
            // 
            this.aLabelDNI.AutoSize = true;
            this.aLabelDNI.Location = new System.Drawing.Point(6, 30);
            this.aLabelDNI.Name = "aLabelDNI";
            this.aLabelDNI.Size = new System.Drawing.Size(31, 15);
            this.aLabelDNI.TabIndex = 0;
            this.aLabelDNI.Text = "DNI:";
            // 
            // tabPageModificar
            // 
            this.tabPageModificar.Location = new System.Drawing.Point(4, 24);
            this.tabPageModificar.Name = "tabPageModificar";
            this.tabPageModificar.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageModificar.Size = new System.Drawing.Size(681, 418);
            this.tabPageModificar.TabIndex = 1;
            this.tabPageModificar.Text = "Modificar";
            this.tabPageModificar.UseVisualStyleBackColor = true;
            // 
            // tabPageEliminar
            // 
            this.tabPageEliminar.Location = new System.Drawing.Point(4, 24);
            this.tabPageEliminar.Name = "tabPageEliminar";
            this.tabPageEliminar.Size = new System.Drawing.Size(681, 418);
            this.tabPageEliminar.TabIndex = 2;
            this.tabPageEliminar.Text = "Eliminar";
            this.tabPageEliminar.UseVisualStyleBackColor = true;
            // 
            // pContratos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlContratos);
            this.Name = "pContratos";
            this.Size = new System.Drawing.Size(689, 446);
            this.Load += new System.EventHandler(this.pContratos_Load);
            this.tabControlContratos.ResumeLayout(false);
            this.tabPageAlta.ResumeLayout(false);
            this.aGroupBoxContrato.ResumeLayout(false);
            this.aGroupBoxContrato.PerformLayout();
            this.aGroupBoxDestino.ResumeLayout(false);
            this.aGroupBoxDestino.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aNumericUpDownHoras)).EndInit();
            this.aGroupBoxSitPro.ResumeLayout(false);
            this.aGroupBoxSitPro.PerformLayout();
            this.aGroupBoxOrigen.ResumeLayout(false);
            this.aGroupBoxOrigen.PerformLayout();
            this.aGroupBoxPersona.ResumeLayout(false);
            this.aGroupBoxPersona.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlContratos;
        private System.Windows.Forms.TabPage tabPageAlta;
        private System.Windows.Forms.TabPage tabPageModificar;
        private System.Windows.Forms.TabPage tabPageEliminar;
        private System.Windows.Forms.GroupBox aGroupBoxPersona;
        private System.Windows.Forms.MaskedTextBox aMaskedTextBoxDNI;
        private System.Windows.Forms.Label aLabelDNI;
        private System.Windows.Forms.TextBox aTextBoxApellido;
        private System.Windows.Forms.Label aLabelApellido;
        private System.Windows.Forms.TextBox aTextBoxNombre;
        private System.Windows.Forms.Label aLabelNombre;
        private System.Windows.Forms.MaskedTextBox aMaskedTextBoxCuit;
        private System.Windows.Forms.Label aLabelCuit;
        private System.Windows.Forms.GroupBox aGroupBoxContrato;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker aDateTimePickerInicio;
        private System.Windows.Forms.DateTimePicker aDateTimePickerBaja;
        private System.Windows.Forms.GroupBox aGroupBoxDestino;
        private System.Windows.Forms.GroupBox aGroupBoxSitPro;
        private System.Windows.Forms.GroupBox aGroupBoxOrigen;
        private System.Windows.Forms.ComboBox aComboBoxOrigen;
        private System.Windows.Forms.CheckBox aCheckBoxAfectado;
        private System.Windows.Forms.Label aLabelCargo;
        private System.Windows.Forms.Label aLabelCondicion;
        private System.Windows.Forms.ComboBox aComboBoxCargo;
        private System.Windows.Forms.ComboBox aComboBoxCondicion;
        private System.Windows.Forms.Label aLabelHoras;
        private System.Windows.Forms.TextBox aTextBoxObs;
        private System.Windows.Forms.Label aLabelObs;
        private System.Windows.Forms.ComboBox aComboBoxFuncion;
        private System.Windows.Forms.Label aLabelFuncion;
        private System.Windows.Forms.ComboBox aComboBoxEquipo;
        private System.Windows.Forms.Label aLabelEquipo;
        private System.Windows.Forms.Label aLabelSede;
        private System.Windows.Forms.TextBox aTextBoxSede;
        internal System.Windows.Forms.Button aButtonCancelar;
        internal System.Windows.Forms.Button aButtonGuardar;
        private System.Windows.Forms.NumericUpDown aNumericUpDownHoras;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
    }
}
